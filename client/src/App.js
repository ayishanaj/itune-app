import SearchBar from "./components/SearchBar";
import "./App.css";

function App() {
  return (
    <div className="App">
      <nav className="nav">
        <a href="./" className="brand">
          Itune Search
        </a>
      </nav>
      <SearchBar />
    </div>
  );
}

export default App;
