import React, { useCallback, useState } from "react";
import _ from "lodash";
import axios from "axios";
import { Loader } from "./loader";
import Detail from "./detail";
import { options } from "./helper";

const SearchBar = () => {
  const [query, setQuery] = useState(null);
  const [optionType, setType] = useState("all");
  const [loading, setLoading] = useState(true);

  const [media, setMediaList] = useState([]);
  const [isEmptyrecords, setRecords] = useState(false);

  const [currentPage, setCurrentPage] = useState(0);
  const [pageCount, setPageCount] = useState(null);

  const handleChange = (event) => {
    debounce(event.target.value,optionType);
  };

  const handleSearchSubmit = async (searchData, type) => {
    try {
      const { data } = await axios.get(
        `https://itunes.apple.com/search?term=${searchData}&media=${type}`
      );
      if (data.resultCount !== 0) {
        setLoading(false);
        setRecords(false);
        setPageCount(Math.ceil(data.resultCount / 10)); 
        setCurrentPage(1)     
      } else {
        setRecords(true);
      }
      setMediaList(data.results);
      setQuery(searchData);
      setType(type);

    } catch (error) {
      setMediaList([]);
      setLoading(true);
    }
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const debounce = useCallback(
    _.debounce((_searchVal,optionType) => {
      handleSearchSubmit(_searchVal, optionType);
    }, 1000),
    []
  );

  const handleSelect = (e) => {
    handleSearchSubmit(query, e.target.value);
  };

  const handlePageClick = (e) => {
    const selectedPage = e.selected + 1;
    setCurrentPage(selectedPage);
  };

  
  const indexOfLastPost = currentPage * 10;
  const indexOfFirstPost = indexOfLastPost - 10;
  const currentMedia = media.slice(indexOfFirstPost, indexOfLastPost);
  return (
    <>
      <form className="moviesearch-form">
        <input
          type="text"
          className="moviesearch-input"
          placeholder="Search your Favourites "
          onChange={handleChange}
        />
        <select
          className="moviesearch-select "
          value={optionType}
          onChange={handleSelect}
        >
          {options.map((item, index) => (
            <option key={index} value={item}>
              {item}
            </option>
          ))}
        </select>
      </form>
      {loading && <Loader title="Search for your favourite songs, audiobooks, movies." />}
      {isEmptyrecords && <Loader title="No Records Found" />}
      {media.length!==0 && <Detail currentMedia={currentMedia} pageCount={pageCount} currentPage={currentPage} handlePageClick={handlePageClick}/>}
    </>
  );
};

export default SearchBar;
