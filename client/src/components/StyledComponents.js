import styled, { keyframes } from "styled-components";

const swishAnimation = (imgUrl) => keyframes`
    0%{
        opacity:1;
        left:-25px;
    }
    /* 17% {
        opacity:0.8;
    }
    33%{
        opacity:0.6;
    }
    83%{
        opacity:0.1; */
    }
    100%{
        opacity:0;  
        left:0px;
    }
`;
export const StyledWelcomeLoader = styled.div`
  align-self: center;
  & img {
    position: relative;
    animation-name: ${swishAnimation};
    animation-duration: 3s;
    animation-iteration-count: infinite;
    opacity: 0;
    left: -50px;
    margin-left: 10px;
    animation-timing-function: cubic-bezier(0, 1, 2, 3);
    /* transition-timing-function: cubic-bezier(0,0,0,3); */

    &.book {
      animation-delay: 0s;
    }
    &.music {
      /* display:none; */
      animation-delay: 1s;
    }
    &.movie {
      /* display:none; */
      animation-delay: 2s;
    }
  }
`;

export const LoaderDiv = styled.div`
  display: flex;
  flex-direction: column-reverse;
  justify-content: center;
  height: 400px;
  font-size: 30px;
  align-items: center;
`;
