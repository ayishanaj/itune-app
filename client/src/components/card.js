import React from "react";

export const Card = ({
  trackName,
  artworkUrl100,
  country,
  kind,
  trackCount,
  longDescription,
  description,
}) => {
  return (
    <div className="movie_component">
      <img
        src={artworkUrl100.replace("100x100", "1200x1200")}
        alt=""
        className="poster"
      />
      <div className="movie-info">
        <h4>{trackName}</h4>
        <span className={`tag`}>
          <strong>{trackCount}</strong>
        </span>
      </div>
      <div className="hover-info">
        <h3>Genere:{kind}</h3>
        <h4>country:{country} </h4>
        <p>{longDescription || description || "No description."}</p>
      </div>
    </div>
  );
};
