import React from "react";
import ReactPaginate from "react-paginate";
import { Card } from "./card";


const Detail = ({currentMedia,pageCount,handlePageClick,currentPage}) => {
  return (
    <div className="container">
          {currentMedia.map((item) => (
            <Card {...item} key={item.id} />
          ))}
          <ReactPaginate
            previousLabel={"prev"}
            nextLabel={"next"}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={pageCount}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            onPageChange={handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
            forcePage={currentPage-1}
          />
    </div>
  );
};

export default Detail;
